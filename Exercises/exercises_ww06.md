# Exercises for ww06


## Exercise 2 - ATmega328 minimal system code

### Information

In this exercise you will work with C code on the ATmega328 and connect the board build from week 05 to it.

You will also work with understanding and documenting C code.

As none of you is experienced in embedded C we've made a sample program to use in your minimum system. The code has comments and explanations about what it does.

### Exercise instructions

1. Clone code from gitlab.com/atmega328-sensor-sample/example-c-code. The repository has 2 branches. Chose the master branch if you are using the Xplained mini development board. Chose the arduino branch if you are using the arduino development board.
2. Read about flowcharts
    * www.lucidchart.com/pages/what-is-a-flowchart-tutorial
3. Make a flowchart of the code. Applications you can use includes:

    * www.yworks.com/products/yed | www.draw.io | www.lucidchart.com
4. Connect the sensor board build from week 05
    * Read in the code which GPIO's to use, the example uses PORTB and one of the analog inputs defined with the variables **btn1**, **led1** and **adcinput**.
    * The [ATmega328 datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf "ATmega328 datasheet link") page 12 has pin names and numbers for reference.
5. Examine which commands you can send via UART
6. Download the code to your development board
    * If you are using Arduino follow the guide "Arduino Uno Atmel Studio guide" (you can find it on itslearning).
    * If you are using the xPlained board follow this [guide](https://www.microchip.com/webdoc/atmega328pxmini/index.html "xPlained mini guide")
8. Use a terminal program ie. Putty to connect to the ATMega 328 via UART serial connection. The serial connection between the board and putty on your computer is established through the USB port regardless of the development board you are using.
    * Read about putty [here](https://store.chipkin.com/articles/using-putty-for-serial-com-connections-hyperterminal-replacement "Putty guide")
    * The serial connection details is in the code comments
9. Send commands to the ATmega328. Verify and document the response.
10. Make a flowchart of the code you are going to use in your project.


## Exercise 3 - Connect ATmega328 and Raspberry Pi

### information

In this exercise you will connect the ATmega328 serial (UART) pins with the RPi serial pins via a level shifter.

_BFW (big fat warning)_: Using 5V for GPIO on raspberry will probably fry your port. Remember to use a level shifter.

### Exercise instructions

The ATMEga will with the supplied example program have UART enabled and functioning. TX and RX pin numbers can be read at page 12 in the ATmega328 datasheet: The [ATmega328 datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf "ATmega328 datasheet link")
They are called RXD and TXD.

On raspberry 3: TX is GPIO14 and RX is GPIO15. See [here](https://elinux.org/RPi_Low-level_peripherals) for more details

#### Connect ATmega328 and raspberry via levelshifter

The ATmega328 GPIO's is 0-5V levels and the raspberry is 0-3.3V levels. The raspberry pins is not 5V tolerant and can suffer damage if they are connected to a 5V pin.
To accomplish connecting the ATmega328 and raspberry we will provide you with a bidirectional level shifter that can convert from one voltage to another.

Read this document to learn more about the [level shifter](https://www.raspberrypi-spy.co.uk/2018/09/using-a-level-shifter-with-the-raspberry-pi-gpio/)

The level shifter you are going to use is this [one](https://www.trab.dk/da/breakout/398-8-kanal-i2c-two-way-bidi-logic-level-converter.html "level converter link")

Connect it like in the below diagram, do not supply any power before everything is connected:

[Link to diagram](https://gitlab.com/atmega328-sensor-sample/schematics-orcad/blob/master/328_lvlshift_RPi.png "ATmega328 to levelshifter to Raspberry Pi connections")

To have the raspberry and the ATMega board talking to each, the raspberry serial port must be set up, and parameters must the same in both ends. We use 9600 8N1

#### Set up raspberry

using configurator:
1. raspi-config
2. enable serial and disable console access

manual:
> enable_uart=1 at the end of /boot/config.txt

If you changed stuff above, you need to reboot.

In the logs you have something like this (this is from a raspberry pi 3)

```
$ dmesg | grep tty
[    0.000000] Kernel command line: 8250.nr_uarts=0 bcm2708_fb.fbwidth=1920 bcm2708_fb.fbheight=1080 bcm2708_fb.fbswap=1 vc_mem.mem_base=0x3ec00000 vc_mem.mem_size=0x40000000  dwc_otg.lpm_enable=0 console=ttyS0,115200 console=tty1 root=PARTUUID=f8cf455f-02 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait quiet splash plymouth.ignore-serial-consoles
[    0.000284] console [tty1] enabled
[    0.744384] 3f201000.serial: ttyAMA0 at MMIO 0x3f201000 (irq = 87, base_baud = 0) is a PL011 rev2
```

`tty1` is claimed by a console, `ttyAMA0` is not. This is as we want it.

We use minicom for the serial connection.
```
apt-get install minicom
```

The raspberry is now set up to use the GPIOs for UART.


#### Connecting

Start minicom
```
minicom -D /dev/ttyAMA0 -b 9600
```

At first you will not be able to see what you type yourself - this relates to "local echo".
In minicom it is "ctrl+A E" to toggle local echo, and then you should be able to see what you type. To exit "ctrl+A X"

Verify by sending some correct commands and some wrong commands.

## exercise 4: Raspberry, atmega and python

1. Fork the repository with the sample code

    if is [here](https://gitlab.com/atmega328-sensor-sample/python-atmega)

2. Make sure the tests work, both locally and on gitlab

    see `test/readme.md` for details

3. Update the `simple_connection.py` to enable connection to the atmega

4. (optional for now) use [argparse](https://docs.python.org/3/library/argparse.html) to be able to supply the serial on the command line.

5. Update the test program

    * Update the ping-pong system to handle reading the temperature and buttons, so it matches the C program in the atmega.
    *

6. Create a new file `atmega.py`

    Implement the following *functions*:
    * read_LED(): LED read status
    * read_temp(): read the temperature ADC value from sensor
    * read_button(): read button status

    Make program blink the LED. Remember the `if __name__ ....` guard

7. Create a simple menu program to test the atmega

    Create a simple console menu program. Like [this one](https://extr3metech.wordpress.com/2014/09/14/simple-text-menu-in-python/)

    Each number will represents a function, like `turn diode on`, `read button` and so on.

    You probably want something like `from atmega import *` at the top of the file.

8. Create a test program that tests the functions of `atmega.py` and put it into gitlab for continual testing.
