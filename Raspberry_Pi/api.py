from flask import Flask, request
from flask_restful import reqparse, abort, Api, Resource
from flask_cors import CORS, cross_origin
from atmega import *

app = Flask(__name__)
CORS(app, resources=r'/*')  # to allow external resources to fetch data
api = Api(app)


@api.resource("/")
class url_index(Resource):
    def get(self):
        returnMessage = {"message": "Yes, it works"}
        return returnMessage


@api.resource("/temp")
class url_temp(Resource):
    def get(self):
        with connect("/dev/ttyS0", 9600) as ser:
            returnMessage = read_temp(ser)
        return returnMessage


@api.resource("/humid")
class url_humid(Resource):
    def get(self):
        with connect("/dev/ttyS0", 9600) as ser:
            returnMessage = read_temp(ser)
        return returnMessage


@api.resource("/btn")
class url_btn(Resource):
    def get(self):
        with connect("/dev/ttyS0", 9600) as ser:
            returnMessage = read_button(ser)
        return returnMessage


@api.resource("/led")
class url_led(Resource):
    def get(self):
        with connect("/dev/ttyS0", 9600) as ser:
            returnMessage = read_led(ser)
        return returnMessage

    def put(self):
        with connect("/dev/ttyS0", 9600) as ser:
            ledState = read_led(ser)
            if "=1" in ledState:
                returnMessage = led1off(ser)
            else:
                returnMessage = led1on(ser)
        return returnMessage


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
