#!/bin/bash



ip="192.168.10.25/24" ## Enter the desired static ip for the RPi
router="192.168.10.1" ## Enter the ip of the router
gitrepourl="https://gitlab.com/sebastianmason/itt2_project_ttt.git" ## Enter the url for the GitLab repository
gitlabNames=(sebastianmason tiha0006 Ducky-Chan) ## Enter the GitLab-username for each of the group-members (separated by spaces)



printf "\ninterface eth0\nstatic ip_address=%s\nstatic routers=%s\nstatic domain_name_servers=1.1.1.1\n" "$ip" "$router" | sudo tee -a /etc/dhcpcd.conf >> /dev/null

sudo ping $router -c 10

sudo wget --spider google.com
if [ $? -eq 0 ]; then
	conn=1
else
	conn=0
fi

if [ $conn -ne 1 ]; then
	sudo reboot
fi

sudo echo "Updating packages"

sudo apt-get update && sudo apt-get upgrade -y

sudo echo "Installing additional packages"

PACKAGES="python3-pip git"
sudo apt-get install $PACKAGES -y

sudo echo "Installing python packages"

sudo pip3 install pyserial

sudo echo "Enabeling SSH"

sudo systemctl enable ssh
sudo systemctl start ssh

sudo echo "Cloning git repository"

sudo git clone $gitrepourl

sudo echo "Adding SSH-key"

for name in ${gitlabNames[*]}
do
    sudo curl https://gitlab.com/$name.keys >> ~/.ssh/authorized_keys
done

sudo mkdir -p ~/.ssh && sudo cat itt2_project_ttt/Raspberry_Pi/Keys/keys | sudo cat >> ~/.ssh/authorized_keys

sudo echo "Enableing UART"

sudo echo "enable_uart=1" | sudo tee -a /boot/config.txt >> /dev/null
sudo systemctl stop serial-getty@ttyS0.service
sudo systemctl disable serial-getty@ttyS0.service
sudo sed -e s/console=serial0,115200//g -i.backup /boot/cmdline.txt

sudo echo "Selfdestruct"

sudo sed -e s/^[^#].*//g -i.backup /etc/rc.local

sudo echo "Done\n\n"