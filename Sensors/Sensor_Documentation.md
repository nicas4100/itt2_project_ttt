# Sensor Details  
Document containing basic information/guidance on the process behind making a veroboard to fit your needs.

## Calculations
![Here](Led_resistor_calculation.png)

## Fritzing
After the calculations we opened up Fritzing as instructed and started gathering the parts from the respective directories, we made a little Veroboard design based on the provided diagrams we were given by our Advisors, we hit save and moved to E-Lab.

## Breadboard tests
After designing the circuit we moved to the E-Lab and began assembling the circuit, after which we used the basic DC power-supply and a multi-meter to check that everything worked as we intended.

![Here](Led_off_bb.png)

![Here](Led_on_bb.png)

## Veroboard
After finishing the breadboard test concluding that our circuit did indeed work as we planned it to we moved everything slightly more compacted onto a Veroboard and soldered everything in place.

![Here](Led_off_vb.png)

![Here](Led_on_vb.png)

## Arduino C test
After writing up a short test script using the Arduino kit we hooked up our finished Veroboard and ran the program, to no surprise it worked as intended.

![Here](Temperature_output.png)
