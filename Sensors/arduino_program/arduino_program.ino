int temp_in = A0;
int button_in = 2;
int led_out = 3;

int temp_val;
float temp_volts;
float temp;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(temp_in, INPUT);
  pinMode(button_in, INPUT);
  pinMode(led_out, OUTPUT);
 }

void loop() {
  // put your main code here, to run repeatedly:
  temp_val = analogRead(temp_in);
  temp_volts = temp_val/204.8;
  temp = (temp_volts-0.5)/0.01;
  //Serial.println("Raw data "+temp_val);
  //Serial.println("Voltage "+String(temp_volts));
  Serial.println("Temperature in Celsius: "+String(temp));
  //delay(2000);

  digitalWrite(led_out,!digitalRead(button_in));

  
}
